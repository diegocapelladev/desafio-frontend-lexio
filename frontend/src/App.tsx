import { Route, Routes } from 'react-router-dom'

import Login from 'pages/Login'
import Dashboard from 'pages/Dashboard'
import { Protected } from 'components/Protected'

const App = () => {
  return (
    <Routes>
      <Route
        path="/dashboard"
        element={
          <Protected>
            <Dashboard />
          </Protected>
        }
      />

      <Route path="/login" element={<Login />} />
      <Route path="/" element={<Login />} />
      <Route path="*" element={<Login />} />
    </Routes>
  )
}

export default App
