import { UserProps } from '.'
import { API } from './../../api/index'

export const setUserLocalStorage = (user: UserProps | null) => {
  localStorage.setItem('user', JSON.stringify(user))
}

export const getUserLocalStorage = () => {
  const token = localStorage.getItem('token')

  if (!token) {
    return null
  }

  const user = JSON.parse(token)

  return user ?? null
}

export const LoginRequest = async (email: string, password: string) => {
  try {
    const request = await API.post('/login', { email, password })

    return request.data
  } catch (err) {
    return null
  }
}
