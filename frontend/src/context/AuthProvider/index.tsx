import { createContext, useEffect, useState } from 'react'
import { getUserLocalStorage, LoginRequest, setUserLocalStorage } from './utils'

export type UserProps = {
  email?: string
  token?: string
}

export type ContextProps = {
  authenticate: (email: string, password: string) => Promise<void>
  logout: () => void
} & UserProps

export type AuthProvideProps = {
  children: JSX.Element
}

export const AuthContext = createContext<ContextProps>({} as ContextProps)

export const AuthProvider = ({ children }: AuthProvideProps) => {
  const [user, setUser] = useState<UserProps | null>()

  useEffect(() => {
    const user = getUserLocalStorage()

    if (user) {
      setUser(user)
    }
  }, [])

  async function authenticate(email: string, password: string) {
    const response = await LoginRequest(email, password)

    const payload = { token: response.token }

    setUser(payload)
    setUserLocalStorage(payload)
  }

  async function logout() {
    setUser(null)
    setUserLocalStorage(null)
  }

  return (
    <AuthContext.Provider value={{ ...user, authenticate, logout }}>
      {children}
    </AuthContext.Provider>
  )
}
