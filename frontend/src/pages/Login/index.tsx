/* eslint-disable @typescript-eslint/no-empty-function */
import { Link, useNavigate } from 'react-router-dom'
import { FormEvent, useState } from 'react'

import AuthImg from 'assets/images/auth.png'
import Logo from 'assets/images/logo.png'

import * as S from './styles'
import Heading from 'components/Heading'
import InputField from 'components/TextField'
import Button from 'components/Button'
import { useAuth } from 'context/AuthProvider/useAuth'
import { toast } from 'react-toastify'

const Login = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const navigate = useNavigate()
  const auth = useAuth()

  const handleSubmit = async (event: FormEvent) => {
    try {
      event.preventDefault()
      await auth.authenticate(email, password)
      navigate('/dashboard')
    } catch (err) {
      return toast.error('Email ou senha, incorreto!')
    }
  }

  return (
    <S.Wrapper>
      <S.BannerBlock>
        <S.ImageBg src={AuthImg} />

        <S.BannerContent>
          <Link to="/">
            <img src={Logo} alt="Logo Lexio" />
          </Link>

          <Heading>
            Criação e gestão de contratos de forma eficiente e descomplicada!
          </Heading>
        </S.BannerContent>
      </S.BannerBlock>

      <S.FormWrapper>
        <S.FormContent>
          <Heading color="gray700">Bem vindo(a)!</Heading>

          <S.FormSubtitle>
            Crie, assine e gerencie contratos como nunca antes{' '}
            <strong>Comece agora!</strong>
          </S.FormSubtitle>
        </S.FormContent>

        <S.Form onSubmit={handleSubmit}>
          <InputField
            type="email"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="exemple@email.com"
          />
          <InputField
            type="password"
            name="password"
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Senha"
          />

          <Button type="submit">Login</Button>
        </S.Form>
      </S.FormWrapper>
    </S.Wrapper>
  )
}

export default Login
