import { CalendarNumber } from '@styled-icons/ionicons-outline'
import { API } from 'api'

import { Container } from 'components/Container'
import Contract, { ContractProps } from 'components/Contract'
import Navbar from 'components/Navbar'
import Search from 'components/Search'
import { useEffect, useState } from 'react'
import * as S from './styles'

const Dashboard = () => {
  const [documents, setDocuments] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const data = await API.get(`/documents`).then((res) => res.data)
      setDocuments(data)
    }
    fetchData()
  }, [])

  return (
    <>
      <Navbar />
      <Container>
        <S.Wrapper>
          <S.Banner>
            <h2>Eventos</h2>
            <S.EventDetails>
              <CalendarNumber />
              <h3>24</h3>
              <p>Eventos da semana</p>
            </S.EventDetails>
          </S.Banner>
          <Search />
          <S.Content>
            {documents.map((doc: ContractProps) => (
              <Contract key={doc.id} {...doc} />
            ))}
          </S.Content>
        </S.Wrapper>
      </Container>
    </>
  )
}
export default Dashboard
