import { Link, useNavigate } from 'react-router-dom'
import { Logout } from '@styled-icons/material-outlined'

import Logo from 'assets/images/logo.png'

import * as S from './styles'
import { useAuth } from 'context/AuthProvider/useAuth'

const Navbar = () => {
  const { logout } = useAuth()
  const navigate = useNavigate()

  const handleLogout = () => {
    logout()

    navigate('/login')
  }

  return (
    <S.Wrapper>
      <S.Container>
        <Link to="/">
          <img src={Logo} alt="" />
        </Link>

        <S.MenuNav>
          <S.MenuItem onClick={() => handleLogout()}>
            Sair <Logout size={25} />
          </S.MenuItem>
        </S.MenuNav>
      </S.Container>
    </S.Wrapper>
  )
}

export default Navbar
