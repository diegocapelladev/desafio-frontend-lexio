import { screen } from '@testing-library/react'
import { renderWithTheme } from 'utils/helpers'

import Button from '.'

describe('<Button />', () => {
  it('should render the heading', () => {
    renderWithTheme(<Button>Login</Button>)

    expect(screen.getByText(/Login/i)).toBeInTheDocument()
  })
})
