import styled, { css } from 'styled-components'

export const Wrapper = styled.header`
  ${({ theme }) => css`
    background-color: ${theme.colors.yellow200};
    color: ${theme.colors.gray800};
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;
    width: 100vw;
    > h1 {
      font-size: 10rem;
    }
    > h2 {
      font-size: ${theme.font.sizes.huge};
    }
    > a {
      text-decoration: none;
      margin-top: 2rem;
      padding: 1rem 2rem;
      background-color: ${theme.colors.gray600};
      color: ${theme.colors.white};
      font-weight: bold;
      border-radius: ${theme.border.radiusX8};
      &:hover {
        filter: brightness(0.9);
      }
    }
  `}
`
