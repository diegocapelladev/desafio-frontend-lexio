import { useAuth } from 'context/AuthProvider/useAuth'
import * as S from './styles'

type ProtectedProps = {
  children: JSX.Element
}

export const Protected = ({ children }: ProtectedProps) => {
  const auth = useAuth()

  if (!auth.token) {
    return (
      <S.Wrapper>
        <h1>403</h1>
        <h2>Acesso negado</h2>
        <a href="/login">Fazer Login</a>
      </S.Wrapper>
    )
  }

  return children
}
