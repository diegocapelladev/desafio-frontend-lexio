import { InputHTMLAttributes } from 'react'
import * as S from './styles'

export type InputFieldProps = {
  type?: string
  name?: string
  value?: string
} & InputHTMLAttributes<HTMLInputElement>

const InputField = ({
  type = 'text',
  name,
  value,
  ...props
}: InputFieldProps) => {
  return (
    <S.Wrapper>
      <S.Input type={type} name={name} value={value} {...props} />
    </S.Wrapper>
  )
}
export default InputField
