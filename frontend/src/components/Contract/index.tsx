/* eslint-disable @typescript-eslint/no-explicit-any */
import { ThreeDotsVertical } from '@styled-icons/bootstrap'

import * as S from './styles'

export type ContractProps = {
  id?: number
  title?: string
  tag?: string
  part?: string
  sub_title?: string
  status?: string
  object?: string
  parties?: []
}

const Contract = ({
  title,
  sub_title,
  status,
  object,
  parties
}: ContractProps) => {
  return (
    <S.Wrapper>
      <S.Name>
        <h2>{title}</h2>
        <p>{sub_title}</p>
      </S.Name>
      <S.Status color={status}>
        <span>&#x2022;</span>
        <p>{status}</p>
      </S.Status>
      <S.Object>
        <strong>Objeto:</strong>
        {object}
      </S.Object>
      <S.ListPart>
        <p>Partes do contrato</p>
        <ul>
          {parties?.map((item: any, index) => (
            <li key={index}>{item.substr(0, 1)}</li>
          ))}
        </ul>
      </S.ListPart>
      <S.Button>
        <ThreeDotsVertical size={25} />
      </S.Button>
    </S.Wrapper>
  )
}

export default Contract
