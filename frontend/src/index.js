import { AuthProvider } from 'context/AuthProvider'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { ToastContainer } from 'react-toastify'

import 'react-toastify/dist/ReactToastify.css'
import GlobalStyles from 'styles/global'
import theme from 'styles/theme'

import App from './App'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
    <AuthProvider>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <ToastContainer />
          <App />
        </BrowserRouter>
        <GlobalStyles />
      </ThemeProvider>
    </AuthProvider>
  </React.StrictMode>
)
