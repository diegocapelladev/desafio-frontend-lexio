# Desafio React LEXIO

## Detalhes

Para rodar o backend do projeto, vá até a pasta `backend` e rode o comando `yarn install` e depois `yarn start`. Um servidor com dados subirá no [http://localhost:8000](http://localhost:8000)

Crie o projeto dentro da pasta `frontend` com a ferramenta que quiser!

## Recursos
Endpoints:
- Login: POST [http://localhost:8000/login](http://localhost:8000/login)
- Buscar Eventos: GET [http://localhost:8000/events](http://localhost:8000/events)
- Buscar Documentos: GET [http://localhost:8000/documents](http://localhost:8000/documents)


## Projeto final

<img src="./capa/capa-1-lexio.png" />

---

<img src="./capa/capa-2-lexio.png" />